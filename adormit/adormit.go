package adormit

import (
	"fmt"
	"github.com/satori/go.uuid"
	"os/exec"
	"reflect"
	"time"
)

func Version() string {
	return "0.0.1a4"
}

var CurrentTimers []Timer
var CurrentAlarms map[string]Alarm

func Init() {
	CurrentAlarms = make(map[string]Alarm)
	CurrentTimers = make([]Timer, 0)
}

type Alarm struct {
	Name    string
	Early   bool
	Time    time.Time
	Active  bool
	Id      string
	Uuid    uuid.UUID
	Creator string
}

func (a *Alarm) String() string {
	return fmt.Sprintf("%v by %v", a.Name, a.Creator)
}

type Timer struct {
	Name     string
	Start    time.Time
	Duration time.Duration
	End      time.Time
	Uuid     uuid.UUID
	Command  string
	Args     []string
	Creator  string
}

func (t *Timer) Countdown() {
	t.Uuid = uuid.Must(uuid.NewV4())
	CurrentTimers = append(CurrentTimers, *t)
	t.Start = time.Now()
	timer1 := time.NewTimer(t.Duration)
	<-timer1.C
	fmt.Println("Timer completed")
	t.End = time.Now()
	cmd := exec.Command(t.Command, t.Args...)
	cmd.Run()
}

func (t Timer) String() string {
	return t.Name
}

func DemoTimer() {
	args := []string{"-i", "clock", "Timer over!", "adormit"}
	t := Timer{Duration: time.Second * 1, Command: "notify-send", Args: args}
	t.Countdown()
}

func debug(ty interface{}) {
	fooType := reflect.TypeOf(ty)
	fmt.Println(fooType)
	for i := 0; i < fooType.NumMethod(); i++ {
		method := fooType.Method(i)
		fmt.Println(method.Name)
	}
}
